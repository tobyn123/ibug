﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iBug
{
    /// <summary>
    /// The Bug class contains all interactions with the bug table.
    /// Each interaction class has a connection method to instantiate a database object and call the LoadDB method.
    /// </summary>
    public class Bug
    {
        #region Initialse
        /// <summary>
        /// The Connection method is only used internally.
        /// Instantiates a db object and calls the LoadDB method. this returns a MySQLConnection datatype.
        /// </summary>
        private SqlConnection Connection()
        {
            db db = new db();
            SqlConnection mySqlConnection = db.LoadDB();
            return mySqlConnection;
        } 
        #endregion

        #region Gets

        /// <summary>
        /// The GetAllBugs method collects all bugs from the database.
        /// Instantiates an sqlConnection using the connection method. then returns an SQLCommand to be used.
        /// </summary>
        public SqlCommand GetAllBugs()
        {
            SqlConnection mySqlConnection = Connection();
            String selcmd = "SELECT Id,title,summary,reprod,expected_results,actual_results,Author,Tester,stage,assigned_to,starts,ends,class,method FROM myTable;";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);
            mySqlConnection.Open();
            return mySqlCommand;

        }

        /// <summary>
        /// The GetBug method collects 1 bug from the database.
        /// Takes in an integer "id" then instantiates an sqlConnection using the connection method. then returns an SQLCommand to be used.
        /// </summary>
        public SqlCommand GetBug(int id)
        {
            SqlConnection mySqlConnection = Connection();
            String selcmd = "SELECT * FROM myTable WHERE Id = @id;";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);
            mySqlCommand.Parameters.AddWithValue("@id", id);
            mySqlConnection.Open();
            return mySqlCommand;

        }

        /// <summary>
        /// The GetUserBugs method collects all bugs assigned to a particular user from the database.
        /// Takes in an integer "userid" then instantiates an sqlConnection using the connection method. then returns an SQLCommand to be used.
        /// </summary>
        public SqlCommand GetUserBugs(int userid)
        {
            SqlConnection mySqlConnection = Connection();
            String selcmd = "SELECT Id,title,summary,reprod,expected_results,actual_results,Author,Tester,stage,starts,ends,class,method  FROM myTable WHERE assigned_to = @userid;";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);
            mySqlCommand.Parameters.AddWithValue("@userid", userid);
            mySqlConnection.Open();
            return mySqlCommand;
        }

        #endregion

        #region Sets
        /// <summary>
        /// The NewBug method adds a new bug to the database.
        /// Takes in all or only the assigned to int. returns a boolean upon completion. True means the row has been added to the database.
        /// </summary>
        public bool NewBug(int assigned_to, string title = null, string summary = null, string reprod = null, string expected_results = null, string actual_results = null, string author = null, string tester = null, string stage = null, string starts = null, string ends = null, string b_class = null, string method = null)
        {

            SqlConnection mySqlConnection = Connection();
            String selcmd1 = "INSERT INTO myTable " +
                "(title, summary, reprod,expected_results,actual_results,author,tester,stage,assigned_to,starts,ends,class,method) " +
                "VALUES" +
                "(@title,@summary,@reprod,@expected_results,@actual_results,@author,@tester,@stage,@assigned_to,@starts,@ends,@class,@method);";

            SqlCommand cmd = new SqlCommand(selcmd1, mySqlConnection);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@summary", summary);
            cmd.Parameters.AddWithValue("@reprod", reprod);
            cmd.Parameters.AddWithValue("@expected_results", expected_results);
            cmd.Parameters.AddWithValue("@actual_results", actual_results);
            cmd.Parameters.AddWithValue("@author", author);
            cmd.Parameters.AddWithValue("@tester", tester);
            cmd.Parameters.AddWithValue("@stage", stage);
            cmd.Parameters.AddWithValue("@assigned_to", assigned_to);
            cmd.Parameters.AddWithValue("@starts", starts);
            cmd.Parameters.AddWithValue("@ends", ends);
            cmd.Parameters.AddWithValue("@class", b_class);
            cmd.Parameters.AddWithValue("@method", method);



            mySqlConnection.Open();
            int iii = cmd.ExecuteNonQuery();

            mySqlConnection.Close();

            if (iii != 0)
            {

                return true;

            }
            else
            {
                return false;
            }



        }

        /// <summary>
        /// The NewBug method edits a bug in the database.
        /// Takes in relevant ID of bug and every other field from the form.
        /// </summary>
        public bool EditBug(object id, int assigned_int, string title = null, string summary = null, string reprod = null, string exp_results = null, string act_results = null, string author = null, string tester = null, string stage = null, string starts = null, string ends = null, string b_class = null, string method = null)
        {

            Console.WriteLine(id);
            SqlConnection mySqlConnection = Connection();
            String selcmd1 = "UPDATE myTable " +
                "SET title = @title" +
                ", summary = @summary" +
                ", reprod = @reprod" +
                ", expected_results = @expected_results" +
                ", actual_results = @actual_results" +
                ", author = @author" +
                ", tester = @tester" +
                ", stage = @stage" +
                ", assigned_to = @assigned_to" +
                ", starts = @starts" +
                ", ends = @ends" +
                ", class = @class" +
                ", method = @method " +
                "WHERE Id = @id;";

            SqlCommand cmd = new SqlCommand(selcmd1, mySqlConnection);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@summary", summary);
            cmd.Parameters.AddWithValue("@reprod", reprod);
            cmd.Parameters.AddWithValue("@expected_results", exp_results);
            cmd.Parameters.AddWithValue("@actual_results", act_results);
            cmd.Parameters.AddWithValue("@author", author);
            cmd.Parameters.AddWithValue("@tester", tester);
            cmd.Parameters.AddWithValue("@stage", stage);
            cmd.Parameters.AddWithValue("@assigned_to", assigned_int);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@starts", starts);
            cmd.Parameters.AddWithValue("@ends", ends);
            cmd.Parameters.AddWithValue("@class", b_class);
            cmd.Parameters.AddWithValue("@method", method);


            mySqlConnection.Open();
            int iii = cmd.ExecuteNonQuery();

            mySqlConnection.Close();

            if (iii != 0)
            {

                return true;

            }
            else
            {
                return false;
            }
        }

        #endregion
    }
    }
