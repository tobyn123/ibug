﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iBug
{
    /// <summary>
    /// The Session class contains a range of data relevant to the user session.
    /// </summary>
    /// <remarks>
    /// Currently not much use for the session class as it could be used as a method inside the user class.
    /// </remarks>
    public static class Session
    {

        public static string username = ""; // Save the session username for display purposes.
        public static string userid = "";   // Save the users ID in the session to load relevant data.


    }
}
