﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iBug
{
    /// <summary>
    /// This is the initial form presented on startup
    /// </summary>
    /// <remarks>
    /// This class simply requests the user details for login.
    /// </remarks>
    public partial class Form1 : Form
    {
        /// <summary>
        /// Initialises component.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            
        }

        /// <summary>
        /// Once the user enters their details the application will check them using the user class and login method.
        /// </summary>
        /// <returns>
        /// If the user credentials are correct a home form will be opened. Otherwise a messageBox will be shown to alert the user the details are incorrect.
        /// </returns>
        private void button1_Click_1(object sender, EventArgs e)
        {
            User user = new User();
            
            if(user.Login(UsernameBox.Text, PasswordBox.Text)) //if returns true
            {
                
                Hide();
                new Home().ShowDialog();
                Close();
            }
            else
            {
                MessageBox.Show("The details you entered were incorrect. Please try again.");

            }



        }
    }
}
