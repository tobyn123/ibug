﻿using System.Configuration;
using System.Data.SqlClient;

namespace iBug
{
    public class db
    {
        public SqlConnection LoadDB()
        {
            
            var connection = ConfigurationManager.ConnectionStrings["ServiceStart"].ConnectionString;
            SqlConnection mySqlConnection = new SqlConnection(connection);

            return mySqlConnection;

        }
    }
}
