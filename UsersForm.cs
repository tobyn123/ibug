﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iBug
{
    /// <summary>
    /// The UserForm class allows administrators to change user details and create or delete user accounts.
    /// </summary>
    public partial class UsersForm : Form
    {
        #region Initialise
        private bool user_selected = false;
        private User user = new User();
        private string user_id = "";
        private string delete_id = "";

        /// <summary>
        /// The UserForm Initialises components
        /// </summary>
        public UsersForm()
        {
            InitializeComponent();
            button2.Visible = false;
            LoadUsers();
        }

        /// <summary>
        /// The LoadUsers method loads all users from the database and binds them to a datagridview.
        /// </summary>
        private void LoadUsers()
        {
            try
            {

                SqlCommand mySqlCommand = user.GetUsers();
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = mySqlCommand;
                BindingSource source = new BindingSource();
                DataTable data = new DataTable();
                sda.Fill(data);
                source.DataSource = data;
                dataGridView1.DataSource = source;
                sda.Update(data);


            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        #endregion


        #region Events
        /// <summary>
        /// Clear fields button
        /// </summary>
        private void button4_Click(object sender, EventArgs e)
        {
            user_selected = false; //Clear text fields and set user selected boolean as false.
            UsernameBox.Clear();
            PasswordBox.Clear();
        }
        /// <summary>
        /// Enter new user button
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            if (user_selected) //User selected boolean prevents user from duplicating a user account.
            {
                MessageBox.Show("You must clear the fields before entering a new user.");
            }
            else
            {
                if (!string.IsNullOrEmpty(UsernameBox.Text)) //make sure fields are not empty
                {
                    if (!string.IsNullOrEmpty(PasswordBox.Text)) //make sure fields are not empty
                    {
                        if (user.NewUser(UsernameBox.Text.ToString(), PasswordBox.Text.ToString()))
                        {
                            MessageBox.Show("The user was added.");
                            UsernameBox.Clear();
                            PasswordBox.Clear();
                            LoadUsers(); //Refresh datagridview data.
                        }
                        else
                        {
                            MessageBox.Show("There was an error please try again.");
                        }
                    }
                    else
                    {
                        MessageBox.Show("A User must have a password.");
                    }
                }
                else
                {
                    MessageBox.Show("A User must have a username.");
                }

            }
        }

        /// <summary>
        /// Edit user button
        /// </summary>
        private void button3_Click(object sender, EventArgs e)
        {
            if (user_selected)
            {
                if (!string.IsNullOrEmpty(UsernameBox.Text)) //make sure fields are not empty
                {
                    if (!string.IsNullOrEmpty(PasswordBox.Text)) //make sure fields are not empty
                    {
                        try
                        {
                            int.TryParse(user_id, out int id); //Try and parse user ID string from datagridview

                            if (user.EditUser(id, UsernameBox.Text.ToString(), PasswordBox.Text.ToString()))
                            {
                                MessageBox.Show("The user was added.");
                                UsernameBox.Clear();
                                PasswordBox.Clear();
                                LoadUsers();
                            }
                            else
                            {
                                MessageBox.Show("There was an error please try again.");
                            }
                        }
                        catch (FormatException fe)
                        {
                            MessageBox.Show(fe.ToString());
                        }

                    }
                    else
                    {
                        MessageBox.Show("A User must have a password.");
                    }
                }
                else
                {
                    MessageBox.Show("A User must have a username.");
                }


            }
            else
            {
                MessageBox.Show("You must select a user to edit before continuing");
            }
        }

        /// <summary>
        /// Delete user button
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {

            try
            {
                int.TryParse(delete_id, out int id);

                if (user.DeleteUser(id))
                {
                    LoadUsers();
                    MessageBox.Show("The user was deleted.");

                }
                else
                {
                    MessageBox.Show("There seems to have been an error.");
                }

            }
            catch (FormatException ex)
            {

            }
        }

        /// <summary>
        /// Select ID from row to allow user to be deleted.
        /// </summary>
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            button2.Visible = true; // Make delete button visible now user is selected.
            delete_id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString(); //Change delete ID variable to that of selected user in table.
        }

        /// <summary>
        /// Place user details into text fields for editing.
        /// </summary>
        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            user_selected = true;
            UsernameBox.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            PasswordBox.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            user_id = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString(); //Change user ID variable to selected user in table.
        } 
        #endregion

    }
}
