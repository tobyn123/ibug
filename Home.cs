﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ScintillaNET;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace iBug
{
    /// <summary>
    /// The Home form is the main window of the application once a user is logged in.
    /// </summary>
    public partial class Home : Form
    {
        #region Initialise

        /*
        Initialise the main objects used on the home form.
        The repo class is initialised globally as it is used by many event methods.
        It is used alongside a boolean declaring wether the repo is live or not.
        */
        private repo repo = new repo();
        private BindingSource source = new BindingSource();
        private DirectoryInfo directoryInfo;
        private bool RepoLive = false;
        private JObject Source;

        /*
        Initialise the Scintilla object
        Scintilla is the text area library for a range of functions.
        Here we are declaring formatting constants.
        Note that colours must be declared as hexadecimal values.
        */
        private Scintilla TextArea = new Scintilla();
        private const int BACK_COLOR = 0x2A211C;
        private const int FORE_COLOR = 0xB7B7B7;
        private const int NUMBER_MARGIN = 1;
        private const int BOOKMARK_MARGIN = 2;
        private const int BOOKMARK_MARKER = 2;
        private const int FOLDING_MARGIN = 3;
        private const bool CODEFOLDING_CIRCULAR = true;
        


        /// <summary>
        /// Initialises component.
        /// </summary>
        public Home()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The method below attaches the Scintilla textarea to the panel created in the designer.
        /// The styling methods are added to initialise formatting.
        /// </summary>
        private void Home_Load(object sender, EventArgs e)
        {
            if (!Session.username.Contains("admin"))
            {
                
                usersToolStripMenuItem1.Visible = false;


            }
            panel1.Controls.Add(TextArea);
            TextArea.WrapMode = WrapMode.None;
            TextArea.IndentationGuides = IndentView.LookBoth;
            // Basic configuration
            TextArea.Dock = System.Windows.Forms.DockStyle.Fill;
            TextArea.TextChanged += (this.OnTextChanged);
            // styling methods.
            InitColors();
            InitSyntaxColoring();
            InitNumberMargin();

            // Load the user and team bugs.
            LoadBugs();
        }

        /// <summary>
        /// The method below Loads all the bugs by initialising a bug class.
        /// the methods GetAllBugs and GetUserBugs are called.
        /// </summary>
        public void LoadBugs()
        {
            try
            {
                Bug bug = new Bug();
                SqlCommand mySqlCommand = bug.GetAllBugs();
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = mySqlCommand;
                BindingSource source = new BindingSource();
                DataTable data = new DataTable();
                sda.Fill(data);
                source.DataSource = data;
                dataGridView1.DataSource = source;
                sda.Update(data);


                int id = Convert.ToInt32(value: Session.userid); //Take the session ID to get the bugs relevant to user.
                BindingSource userSource = new BindingSource();
                DataTable userBugData = new DataTable();
                SqlCommand userCommand = bug.GetUserBugs(id);
                SqlDataAdapter userSDA = new SqlDataAdapter
                {
                    SelectCommand = userCommand
                };
                userSDA.Fill(userBugData);
                userSource.DataSource = userBugData;
                dataGridView2.DataSource = userSource;
                userSDA.Update(userBugData);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        #endregion

        #region Formatting

        private void InitColors()
        {

            TextArea.SetSelectionBackColor(true, IntToColor(0x114D9C));

        }

        private void InitNumberMargin()
        {

            TextArea.Styles[Style.LineNumber].BackColor = IntToColor(BACK_COLOR);
            TextArea.Styles[Style.LineNumber].ForeColor = IntToColor(FORE_COLOR);
            TextArea.Styles[Style.IndentGuide].ForeColor = IntToColor(FORE_COLOR);
            TextArea.Styles[Style.IndentGuide].BackColor = IntToColor(BACK_COLOR);

            var nums = TextArea.Margins[NUMBER_MARGIN];
            nums.Width = 30;
            nums.Type = MarginType.Number;
            nums.Sensitive = true;
            nums.Mask = 0;

            TextArea.MarginClick += TextArea_MarginClick;
        }

        private void InitSyntaxColoring()
        {

            // Configure the default style
            TextArea.StyleResetDefault();
            TextArea.Styles[Style.Default].Font = "Consolas";
            TextArea.Styles[Style.Default].Size = 10;
            TextArea.Styles[Style.Default].BackColor = IntToColor(0x212121);
            TextArea.Styles[Style.Default].ForeColor = IntToColor(0xFFFFFF);
            TextArea.StyleClearAll();

            // Configure the CPP (C#) lexer styles
            TextArea.Styles[Style.Cpp.Identifier].ForeColor = IntToColor(0xD0DAE2);
            TextArea.Styles[Style.Cpp.Comment].ForeColor = IntToColor(0xBD758B);
            TextArea.Styles[Style.Cpp.CommentLine].ForeColor = IntToColor(0x40BF57);
            TextArea.Styles[Style.Cpp.CommentDoc].ForeColor = IntToColor(0x2FAE35);
            TextArea.Styles[Style.Cpp.Number].ForeColor = IntToColor(0xFFFF00);
            TextArea.Styles[Style.Cpp.String].ForeColor = IntToColor(0xFFFF00);
            TextArea.Styles[Style.Cpp.Character].ForeColor = IntToColor(0xE95454);
            TextArea.Styles[Style.Cpp.Preprocessor].ForeColor = IntToColor(0x8AAFEE);
            TextArea.Styles[Style.Cpp.Operator].ForeColor = IntToColor(0xE0E0E0);
            TextArea.Styles[Style.Cpp.Regex].ForeColor = IntToColor(0xff00ff);
            TextArea.Styles[Style.Cpp.CommentLineDoc].ForeColor = IntToColor(0x77A7DB);
            TextArea.Styles[Style.Cpp.Word].ForeColor = IntToColor(0x48A8EE);
            TextArea.Styles[Style.Cpp.Word2].ForeColor = IntToColor(0xF98906);
            TextArea.Styles[Style.Cpp.CommentDocKeyword].ForeColor = IntToColor(0xB3D991);
            TextArea.Styles[Style.Cpp.CommentDocKeywordError].ForeColor = IntToColor(0xFF0000);
            TextArea.Styles[Style.Cpp.GlobalClass].ForeColor = IntToColor(0x48A8EE);

            TextArea.Lexer = Lexer.Cpp;

            TextArea.SetKeywords(0, "class extends implements import interface new case do while else if for in switch throw get set function var try catch finally while with default break continue delete return each const namespace package include use is as instanceof typeof author copy default deprecated eventType example exampleText exception haxe inheritDoc internal link mtasc mxmlc param private return see serial serialData serialField since throws usage version langversion playerversion productversion dynamic private public partial static intrinsic internal native override protected AS3 final super this arguments null Infinity NaN undefined true false abstract as base bool break by byte case catch char checked class const continue decimal default delegate do double descending explicit event extern else enum false finally fixed float for foreach from goto group if implicit in int interface internal into is lock long new null namespace object operator out override orderby params private protected public readonly ref return switch struct sbyte sealed short sizeof stackalloc static string select this throw true try typeof uint ulong unchecked unsafe ushort using var virtual volatile void while where yield");
            TextArea.SetKeywords(1, "void Null ArgumentError arguments Array Boolean Class Date DefinitionError Error EvalError Function int Math Namespace Number Object RangeError ReferenceError RegExp SecurityError String SyntaxError TypeError uint XML XMLList Boolean Byte Char DateTime Decimal Double Int16 Int32 Int64 IntPtr SByte Single UInt16 UInt32 UInt64 UIntPtr Void Path File System Windows Forms ScintillaNET");

        }

        /// <summary>
        /// Converts and rgb int to an rgb colour value.
        /// </summary>
        public static Color IntToColor(int rgb) => Color.FromArgb(255, (byte)(rgb >> 16), (byte)(rgb >> 8), (byte)rgb);

        private void dataGridView1_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString() == "Red")
            {
                e.CellStyle.BackColor = Color.Red;

            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString() == "Amber")
            {
                e.CellStyle.BackColor = Color.Yellow;
            }
            else if (dataGridView1.Rows[e.RowIndex].Cells[8].Value.ToString() == "Green")
            {
                e.CellStyle.BackColor = Color.Green;
            }


        }

        private void dataGridView2_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView2.Rows[e.RowIndex].Cells[8].Value.ToString() == "Red")
            {
                e.CellStyle.BackColor = Color.Red;

            }
            else if (dataGridView2.Rows[e.RowIndex].Cells[8].Value.ToString() == "Amber")
            {
                e.CellStyle.BackColor = Color.Yellow;
            }
            else if (dataGridView2.Rows[e.RowIndex].Cells[8].Value.ToString() == "Green")
            {
                e.CellStyle.BackColor = Color.Green;
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// When called this method runs the loadbugs method.
        /// For some reason I was not able to call LoadBugs outside this class however this method seems to work.
        /// </summary>
        public void RefreshData()
        {
            LoadBugs();
        }

        private void OnTextChanged(Object sender, EventArgs e)
        {

        }

        private void LoadDataFromFile(string path)
        {

            if (File.Exists(path))
            {

                groupBox2.Text = Path.GetFileName(path);
                TextArea.Text = File.ReadAllText(path);


            }

        }

        private void TextArea_MarginClick(object sender, MarginClickEventArgs e)
        {
            if (e.Margin == BOOKMARK_MARGIN)
            {
                // Do we have a marker for this line?
                const uint mask = (1 << BOOKMARK_MARKER);
                var line = TextArea.Lines[TextArea.LineFromPosition(e.Position)];
                if ((line.MarkerGet() & mask) > 0)
                {
                    // Remove existing bookmark
                    line.MarkerDelete(BOOKMARK_MARKER);
                }
                else
                {
                    // Add bookmark
                    line.MarkerAdd(BOOKMARK_MARKER);
                }
            }
        }
        

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            /*
            When a user clicks any cell in a particular row (thus selecting that row),
            the open document's first visible line will change to the "starts" value of the row.
            */
            try
            {
                int.TryParse(s: dataGridView2.Rows[e.RowIndex].Cells[9].Value.ToString(), result: out int line); // Trying to parse the value in the field.
                if (line <= 0) // if the value is less than or equal to 0 make the first line default to 1
                {
                    TextArea.FirstVisibleLine = 1;
                }
                else
                {
                    TextArea.FirstVisibleLine = line - 1;// else the value minus 1. This is because it will actually hide the line selected as first visible line.
                }
            }
            catch (ArgumentOutOfRangeException) // If the value is not a number reset the line number to 1.
            {
                TextArea.FirstVisibleLine = 1;

            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            /*
            When a user clicks any cell in a particular row (thus selecting that row),
            the open document's first visible line will change to the "starts" value of the row.
            */
            try
            {

                int.TryParse(s: dataGridView1.Rows[e.RowIndex].Cells[10].Value.ToString(), result: out int line);// Trying to parse the value in the field.
                if (line <= 0)// if the value is less than or equal to 0 make the first line default to 1
                {
                    TextArea.FirstVisibleLine = 1;
                }
                else
                {
                    TextArea.FirstVisibleLine = line - 1;// else the value minus 1. This is because it will actually hide the line selected as first visible line.
                }
            }
            catch (ArgumentOutOfRangeException) // If the value is not a number reset the line number to 1.
            {
                TextArea.FirstVisibleLine = 1;

            }


        }

        private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            /*
           When a user clicks double clicks a row the corresponding id from the table is parsed as an int and sent to the bugform.
           This is so the user can edit the bug they have selected.
           */
            int.TryParse(dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString(), out int id);

            // Create a new instance of the bug class
            BugForm bugForm = new BugForm(id);

            // Show the bug form
            bugForm.Show();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            /*
            When a user clicks double clicks a row the corresponding id from the table is parsed as an int and sent to the bugform.
            This is so the user can edit the bug they have selected.
            */
            
            int.TryParse(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString(), out int id);

            // Create a new instance of the bug class
            BugForm bugForm = new BugForm(id);

            // Show the bug form
            bugForm.Show();
        }

        private void treeView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            /*
            After a user has loaded a project they can navigate the project tree to access files. 
            They can either choose to connect to the repository or choose a local folder.
            TreeNodes are to behave differently depending as the source path given is different in both cases.
            if the user has selected a repo then a boolean is set called "RepoLive" otherwise it will continue to attempt to load local file.

            */
            if (RepoLive)
            {
                TreeNode CurrentNode = treeView1.SelectedNode; // Get the selected node.

                string name = CurrentNode.Text; //get the name to match to the file path.

                foreach (var item in Source["response"]) //navigate through the json to find the matching name and then select the path.
                {
                    if (item["path"].ToString() == name)
                    {
                        string text = repo.GET(item["href"].ToString()); //get the contents of the link using the repo GET method
                        TextArea.Text = text; // Display contents in textarea.
                    }
                }
                
            }
            else
            {

                TreeNode CurrentNode = treeView1.SelectedNode; // Get the selected node.
                string fullpath = CurrentNode.FullPath.Replace("File Explorer\\", String.Empty); // Create a string of the path stored in the node.
                string dir = directoryInfo + "\\" + fullpath; // complete the path.
                LoadDataFromFile(dir); //Call the load datafromfile method with the string path.
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void linkRepoToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string url = "https://api.bitbucket.org/2.0/repositories/tobyn123/ibug/src"; // Repo 
            Source = repo.getRepoSrc(url); // get the source code from the repo.

            RepoLive = true; // set the repolive bool as true to prevent the nodes being intitialsed incorrectly.

            if (treeView1.Nodes[0].Nodes.Count > 0)
            {
                treeView1.Nodes[0].Nodes.Clear();
            }

            foreach (var item in Source["response"])
            {

                TreeNode node = treeView1.Nodes[0].Nodes.Add(item["path"].ToString());
                node.ImageIndex = node.SelectedImageIndex = 2;

            }

            textBox1.Text = url;

        }

        private void loadSingleFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream stream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK && (stream = openFileDialog1.OpenFile()) != null)
            {
                LoadDataFromFile(openFileDialog1.FileName);


            }
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create a new instance of the bug class
            BugForm bugForm = new BugForm();

            // Show the settings form
            bugForm.Show();
        }

        private void loadLocalProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RepoLive)
            {
                RepoLive = false; //if the repo has been used already set the bool to false.
            }
            using (FolderBrowserDialog fbd = new FolderBrowserDialog() { Description = "Select Your Project Path:" }) //using folder browser.
            {
                if (treeView1.Nodes[0].Nodes.Count > 0) //Reset nodes if they are already set.
                {
                    treeView1.Nodes[0].Nodes.Clear();

                }
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    textBox1.Text = fbd.SelectedPath; //get the selected project path.


                    directoryInfo = new DirectoryInfo(fbd.SelectedPath); //get directory info

                    if (directoryInfo.Exists) //check if path exists.
                    {

                        try
                        {
                            DirectoryInfo[] directories = directoryInfo.GetDirectories(); //get child directories (folders) of the path.
                            if (directories.Length > 0)
                            {
                                
                                foreach (DirectoryInfo directory in directories) //foreach directory in the path
                                {

                                    TreeNode node = treeView1.Nodes[0].Nodes.Add(directory.Name); //set a node for each directory using it's name
                                    node.ImageIndex = node.SelectedImageIndex = 1;
                                    foreach (FileInfo file in directory.GetFiles()) //foreach file in a directory
                                    {

                                        if (file.Exists) //make sure file exists.
                                        {
                                            TreeNode nodes = treeView1.Nodes[0].Nodes[node.Index].Nodes.Add(file.Name); //add child node for file.
                                            nodes.ImageIndex = nodes.SelectedImageIndex = 2; //set the icon of the node.
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }
                        treeView1.Nodes[0].Expand();
                    }
                    else
                    {

                    }

                }
            }
        }
        #endregion

        private void addToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            UsersForm uf = new UsersForm();
            uf.ShowDialog();
        }
    }
}
