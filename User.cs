﻿using System;
using System.Data.SqlClient;

namespace iBug
{
    /// <summary>
    /// The User class contains all interactions with the user table.
    /// Each interaction class has a connection method to instantiate a database object and call the LoadDB method.
    /// </summary>
    /// <remarks>
    /// Shouold be later developed to house session method.
    /// </remarks>
    public class User
    {
        #region Initialise
        /// <summary>
        /// The Connection method is only used internally.
        /// Instantiates a db object and calls the LoadDB method. this returns a MySQLConnection datatype.
        /// </summary>
        private SqlConnection Connection()
        {
            db db = new db();
            SqlConnection mySqlConnection = db.LoadDB();
            return mySqlConnection;
        }

        #endregion

        #region UserControl

        /// <summary>
        /// The Login method takes credentials and compares them with database information to allow user access.
        /// Instantiates an sqlConnection using the connection method. selects from the database where username and password match that of the user entry,
        /// if the selection returns nothing the details are incorrect and the method returns false. Otherwise it returns true.
        /// </summary>
        public Boolean Login(string username, string password)
        {
            SqlConnection mySqlConnection = Connection();
            String selcmd1 = "SELECT * FROM users WHERE username = @username AND password = @password;";
            SqlCommand cmd = new SqlCommand(selcmd1, mySqlConnection);
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);

            mySqlConnection.Open();

            var result = cmd.ExecuteScalar();
            if (result != null) // If result returns a user...
            {
                SqlDataReader myReader = cmd.ExecuteReader();
                while (myReader.Read())
                {
                    Session.userid = myReader["Id"].ToString();     // Set Session user ID as users ID
                    Session.username = myReader["username"].ToString(); // Set Session usernam as users username
                }
                mySqlConnection.Close();
                return true; //return true

            }
            else
            {
                mySqlConnection.Close();
                return false;
            }



        }

        #endregion

        #region Gets

        /// <summary>
        /// The GetUsers method collects all users from the database.
        /// Instantiates an sqlConnection using the connection method. then returns an SQLCommand to be used.
        /// </summary>
        public SqlCommand GetUsers()
        {
            SqlConnection mySqlConnection = Connection();
            String selcmd = "SELECT * FROM users;";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);
            mySqlConnection.Open();
            return mySqlCommand;

        }



        /// <summary>
        /// The GetUserIds method returns a list of user IDs for use in the application
        /// Instantiates an sqlConnection using the connection method. then returns an SQLCommand to be used.
        /// </summary>
        public SqlCommand GetUserIds()
        {
            SqlConnection mySqlConnection = Connection();
            String selcmd = "SELECT Id,username FROM users;";
            SqlCommand mySqlCommand = new SqlCommand(selcmd, mySqlConnection);
            mySqlConnection.Open();
            return mySqlCommand;
        }

        #endregion

        #region SETS

        /// <summary>
        /// The NewUser method createsa new user and adds the details to the database.
        /// Boolean, returns true if user is created.
        /// </summary>
        public bool NewUser(string username, string password)
        {
            SqlConnection mySqlConnection = Connection();
            String selcmd1 = "INSERT INTO users " +
                "(username,password) " +
                "VALUES" +
                "(@user,@pass);";

            SqlCommand cmd = new SqlCommand(selcmd1, mySqlConnection);
            cmd.Parameters.AddWithValue("@user", username);
            cmd.Parameters.AddWithValue("@pass", password);

            mySqlConnection.Open();
            int res_meta = cmd.ExecuteNonQuery();

            mySqlConnection.Close();

            if (res_meta != 0)
            {

                return true;

            }
            else
            {
                return false;
            }


        }

        public bool EditUser(int id, string username, string password)
        {
            SqlConnection mySqlConnection = Connection();
            String selcmd1 = "UPDATE users " +
                "SET username = @user, password = @pass" +
                " WHERE Id = @id;";

            SqlCommand cmd = new SqlCommand(selcmd1, mySqlConnection);
            cmd.Parameters.AddWithValue("@user", username);
            cmd.Parameters.AddWithValue("@pass", password);
            cmd.Parameters.AddWithValue("@id", id);

            mySqlConnection.Open();
            int res_meta = cmd.ExecuteNonQuery();

            mySqlConnection.Close();

            if (res_meta != 0)
            {

                return true;

            }
            else
            {
                return false;
            }


        }

        public bool DeleteUser(int id)
        {
            SqlConnection mySqlConnection = Connection();
            String selcmd1 = "DELETE FROM myTable WHERE assigned_to = @id";
            SqlCommand cmd = new SqlCommand(selcmd1, mySqlConnection);
            cmd.Parameters.AddWithValue("@id", id);
            mySqlConnection.Open();
            mySqlConnection.Close();
            String selcmd2 = "DELETE FROM users WHERE Id = @id";
            SqlCommand cmd1 = new SqlCommand(selcmd2, mySqlConnection);
            cmd1.Parameters.AddWithValue("@id", id);
            mySqlConnection.Open();
            int res_meta = cmd1.ExecuteNonQuery();
            mySqlConnection.Close();
            if (res_meta != 0)
            {
                return true;
            }
            else
            {
                return false;
            }


        }


        #endregion
    }
}