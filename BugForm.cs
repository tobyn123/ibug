﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iBug
{
    /// <summary>
    /// The Bug form allows a user to create or edit a bug.
    /// </summary>
    public partial class BugForm : Form
    {

        #region Initialise
        /*
        Initialise the main objects used on the bug form.
        We need Home object for later use.
        */
        Home obj = (Home)Application.OpenForms["Home"];
        private int id2;

        /// <summary>
        /// The Bug form allows a user to create or edit a bug.
        /// If the user double clicks a row in the table on the homepage the bug form will be instantiated with a bug ID to edit that bug.
        /// Otherwise the bug form will be loaded blank for entering a new bug.
        /// </summary>
        public BugForm(int id = 0) //default bug id is 0.
        {
            InitializeComponent();

            User user = new User(); //Instantiate a new user object.
            SqlCommand cmd = user.GetUserIds(); //call GetUserIds method
            SqlDataAdapter sda = new SqlDataAdapter();
            sda.SelectCommand = cmd; //attach returned data to adapter.
            DataSet ds = new DataSet();
            sda.Fill(ds);
            cmd.ExecuteNonQuery();
            comboBox1.DisplayMember = "username";
            comboBox1.ValueMember = "Id";
            comboBox1.DataSource = ds.Tables[0]; //attach dataset to combobox on form.
            comboBox1.Enabled = true;

            if (id != 0)
            {
                /*
                If the user is editing a bug we use the passed ID to get the bug details and fill the form fields with the data.
                */
                button1.Visible = false; //hide the "save new bug" button
                Bug b = new Bug(); // new bug object
                SqlCommand cmdEditBug = b.GetBug(id); //get the bug using the passed in ID
                SqlDataReader reader = cmdEditBug.ExecuteReader(); //read the data

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        /*
                        Add the read data to the textboxes on the form.
                        */
                        id2 = Int32.Parse(reader["Id"].ToString());

                        textBox1.Text = reader["title"].ToString();
                        textBox4.Text = reader["summary"].ToString();
                        textBox5.Text = reader["reprod"].ToString();
                        textBox6.Text = reader["expected_results"].ToString();
                        textBox7.Text = reader["actual_results"].ToString();
                        textBox2.Text = reader["Author"].ToString();
                        textBox3.Text = reader["Tester"].ToString();

                        //Check the corresponding radio button.
                        if (reader["stage"].ToString() == "Green")
                        {
                            radioButton3.Checked = true;

                        }
                        else if (reader["stage"].ToString() == "Amber")
                        {
                            radioButton2.Checked = true;

                        }
                        else if (reader["stage"].ToString() == "Red")
                        {
                            radioButton1.Checked = true;
                        }
                        comboBox1.SelectedValue = reader["assigned_to"].ToString(); //set the selected combobox value as the corresponding read data.
                    }
                    reader.Close();
                }


            }
            else
            {
                button2.Visible = false; // hide the "edit bug" button.
            }


        }

        #endregion

        #region Events

        // Create Bug button
        private void button1_Click(object sender, EventArgs e)
        {
            /*
            If the new bug button is selected we take the contents of the form fields and turn them into string variables.
            */
            Bug b = new Bug();
            string title = textBox1.Text;
            string summary = textBox4.Text;
            string reprod = textBox5.Text;
            string exp_results = textBox6.Text;
            string act_results = textBox7.Text;
            string author = textBox2.Text;
            string tester = textBox3.Text;
            string starts = textBox10.Text;
            string ends = textBox9.Text;
            string b_class = textBox8.Text;
            string method = textBox11.Text;
            string assigned_to = comboBox1.SelectedValue.ToString();
            int assigned_int = Convert.ToInt32(assigned_to);

            String stage;

            if (radioButton1.Checked == true)
            {
                stage = radioButton1.Text;
            }
            else if (radioButton2.Checked == true)
            {
                stage = radioButton2.Text;

            }
            else if (radioButton3.Checked == true)
            {
                stage = radioButton3.Text;
            }
            else
            {
                stage = "";
            }

            /*
             * call the newbug method from the bug object and if returns true data has been added to the database.
             */
            if (b.NewBug(assigned_int, title, summary, reprod, exp_results, act_results, author, tester, stage, starts, ends, b_class, method))
            {
                obj.RefreshData(); //refresh the data inside the gridviews on the home form.
                MessageBox.Show("The bug has been reported."); //alert to the user success.
            }
            else
            {
                MessageBox.Show("There was an error reporting the bug, please try again.");
            }





        }

        //Edit Bug Button
        private void button2_Click(object sender, EventArgs e)
        {
            Bug b = new Bug();
            string title = textBox1.Text;
            string summary = textBox4.Text;
            string reprod = textBox5.Text;
            string exp_results = textBox6.Text;
            string act_results = textBox7.Text;
            string author = textBox2.Text;
            string tester = textBox3.Text;
            string starts = textBox10.Text;
            string ends = textBox9.Text;
            string b_class = textBox8.Text;
            string method = textBox11.Text;
            string assigned_to = comboBox1.SelectedValue.ToString();
            int assigned_int = Convert.ToInt32(assigned_to);

            String stage;

            if (radioButton1.Checked == true)
            {
                stage = radioButton1.Text;


            }
            else if (radioButton2.Checked == true)
            {
                stage = radioButton2.Text;


            }
            else if (radioButton3.Checked == true)
            {
                stage = radioButton3.Text;


            }
            else
            {
                stage = "";
            }

            int id = id2;

            if (b.EditBug(id, assigned_int, title, summary, reprod, exp_results, act_results, author, tester, stage, starts, ends, b_class, method))
            {
                obj.RefreshData();
                MessageBox.Show("The bug has been updated.");
            }
            else
            {
                MessageBox.Show("There was an error reporting the bug, please try again.");
            }
        }

        #endregion

    }
}
