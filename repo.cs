﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace iBug
{
    /// <summary>
    /// The repo class contains all available features for interacting with a repository.
    /// </summary>
    public class repo
    {
        #region Gets

        /// <summary>
        /// The GET method takes in a string url and creates a web request with it. It then handles a web response and parses any data returned to a string.
        /// </summary>
        public string GET(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                WebResponse response = request.GetResponse();
                using (Stream responseStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    return reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }

        // POST a JSON string
        public void POST(string url, string jsonContent)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }
            long length = 0;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    length = response.ContentLength;
                }
            }
            catch (WebException ex)
            {
                
            }
        }

        /// <summary>
        /// Returns a JObject.
        /// Th method takes in a string url and uses the GET method to return a websresponse as a string and converts that string to a JSON
        /// This will only work on BitBucket src responses.
        /// Any file ending with .cs in the response is stored.
        /// </summary>
        public JObject getRepoSrc(string url)
        {

            JObject Commits = JObject.Parse(GET(url));
            string JsonOut = "{\"response\": [";
            int pagelen;
            int.TryParse(Commits["pagelen"].ToString(), out pagelen);
            for (int i = 0; i < pagelen; i++)
            {
                JObject ContinuedCommits = JObject.Parse(GET(url + "?page=" + (i)));

                foreach (var package in ContinuedCommits["values"])
                {
                    string path = package["path"].ToString();

                    if (path.EndsWith(".cs"))
                    {
                        string href = package["links"]["self"]["href"].ToString();
                        JsonOut += "{\"path\": \"" + path + "\",\"href\": \"" + href + "\"},";
                    }
                }
            }

            JsonOut += "]}";
            JObject JParse = JObject.Parse(JsonOut);
            return JParse;

        }





        #endregion

    }
}
